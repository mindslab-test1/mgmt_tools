#!/bin/bash
# Update the uid if an user already exists, else add the user

USERNAME=$1
USER_ID=$2

if grep -q "^${USERNAME}:" /etc/passwd ; then
    echo "User ${USERNAME} already exists."
    OLD_UID=$(id -u ${USERNAME})
    usermod -g aisci -G aisci,docker ${USERNAME}
    if [ ${OLD_UID} != ${USER_ID} ] ; then
	echo "Updating uid of ${USERNAME} to ${USER_ID} (from ${OLD_UID})"
        usermod -u ${USER_ID} ${USERNAME}
        if  [ $? -eq 0 ] && [ ${UPDATE_ALL_FILES} = true ] ; then
            echo "Now chown-ing files owned by the user"
            find / \( $(sed 's/^/-path /g; ' "${EXCLUDE_INFO}" |\
                sed ':a; N; $!ba; s/\n/ -o /g;') \) -prune -o -uid ${OLD_UID} -exec chown -h ${USERNAME}:aisci {} \;
        fi
    fi 
else
    echo "Adding an user ${USERNAME} with uid ${USER_ID}"
    bash scripts/add_user.sh ${USERNAME} ${USER_ID}
fi
