#!/bin/bash
# Update the gid if a group already exists, else add the group

GROUPNAME=$1
GROUP_ID=$2

if grep -q "^${GROUPNAME}:" /etc/group ; then
    echo "Group ${GROUPNAME} already exists."
    OLD_GID=$(cut -d: -f3 < <(getent group ${GROUPNAME}))
    if [ ${OLD_GID} != ${GROUP_ID} ] ; then
	echo "Updating gid of ${GROUPNAME} to ${GROUP_ID} (from ${OLD_GID})"
        groupmod -g ${GROUP_ID} ${GROUPNAME}
        if  [ $? -eq 0 ] && [ ${UPDATE_ALL_FILES} = true ] ; then
            echo "Now chgrp-ing files managed by the group"
	    find / \( $(sed 's/^/-path /g; ' "${EXCLUDE_INFO}" |\
       	        sed ':a; N; $!ba; s/\n/ -o /g;') \) -prune -o -gid ${OLD_GID} -exec chgrp -h ${GROUPNAME} {} \;
	fi
    fi
else
    echo "Adding a group ${GROUPNAME} with gid ${GROUP_ID}"
    groupadd -g ${GROUP_ID} ${GROUPNAME}
fi
