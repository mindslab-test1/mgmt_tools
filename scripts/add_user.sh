#!/bin/bash
# Usage:
#   $ bash add_user.sh USERNAME USER_ID
#   $ passwd USERNAME
# 
# Inform the user to run below to change password after his/her first login.
#   $ passwd

USERNAME=$1
USER_ID=$2

useradd \
    -u ${USER_ID} \
    -G aisci,docker \
    -g aisci \
    -s /bin/bash \
    -m \
    ${USERNAME}

PASSWORD="${USERNAME}${USER_ID}${PW_SUFFIX}"
echo -e "${PASSWORD}\n${PASSWORD}" | passwd ${USERNAME}
