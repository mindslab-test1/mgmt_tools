#!/bin/bash

# Reading .info files and make accounts according to them
GROUPS_INFO="infos/groups.info"
USERS_INFO="users_all_temporary.info"
EXCLUDE_INFO="infos/exclude_paths.info"
export EXCLUDE_INFO

echo ""
echo "================================================="
echo "Syncing accounts(uid/gid) for Brain AI Scientists"
echo "================================================="
echo ""

PW_SUFFIX=$1
UPDATE_ALL_FILES=$2


# Setting password suffix
if [ -z ${PW_SUFFIX} ]; then
    read -p "> Password suffix(default: @): " PW_SUFFIX
fi
PW_SUFFIX=${PW_SUFFIX:-@}
echo "Default password for all accounts will be {username}{uid}${PW_SUFFIX}. You may use passwd later.\n"
export PW_SUFFIX


# Confirming to update whether all files or only files under home directories.
if [ -z ${UPDATE_ALL_FILES} ]; then
    read -p "> CAUTION!! Update all files' owner & group. It may take much time (Y/n)? " UPDATE_ALL_FILES
fi
UPDATE_ALL_FILES=${UPDATE_ALL_FILES:-Y}

if [ ${UPDATE_ALL_FILES} == Y ] || [ ${UPDATE_ALL_FILES} == y ]; then
    export UPDATE_ALL_FILES=true
elif [ ${UPDATE_ALL_FILES} == N ] || [ ${UPDATE_ALL_FILES} == n ]; then
    export UPDATE_ALL_FILES=false
else 
    exit
fi
echo ${PW_SUFFIX} ${UPDATE_ALL_FILES}
export UPDATE_ALL_FILES

# Sync groups
echo "== Syncing groups =="
while IFS=: read -r group_name group_id ; do
    echo "> Updating ${group_name}"
    bash scripts/update_group.sh ${group_name} ${group_id}
done < ${GROUPS_INFO}


# Sync users
cat $(find -name "users_*.info") > "${USERS_INFO}"
echo "== Syncing users =="
while IFS=: read -r user_name user_id ; do
    echo "> Updating ${user_name}"
    bash scripts/update_user.sh ${user_name} ${user_id}
done < ${USERS_INFO} 
rm ${USERS_INFO}
