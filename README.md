Brain UID/GID synchronization tool
==================================

## How to sync
### Preparation
- The affected users should not be running any process. An user will not be affected if one's uid or gid is not being changed.
- Check `infos/users_*.info` and `infos/groups.info` for the correctness of informations.
- Check `infos/exclude_path.info` for excluding certain directories or files when updating uid/gid recursively from the root(`/`).
  - **CAUTION!** Be careful not to update shared(network-attached) files. You may append those paths to the `infos/exclude_path.info`.

### in a server (with sudo-able account)
- `$ sudo su`
- `$ COMMAND` (see below)

### in a docker container (when you are already a root)
- `$ COMMAND` (see below)
- `$ su your_account`


### **COMMAND**
- `$ bash sync_accounts PW_SUFFIX UPDATE_ALL_FILES`
  - `PW_SUFFIX`: character(s). All passwords will be set as `uname;uid;PW_SUFFIX`. ex) `brain31234@`
  - `UPDATE_ALL_FILES`: Y/N. If Y, all files under `/` will be changed recursively.
    - it can't be applied for already broken(lost its mapping) ids.
    - files/directories under the paths in `infos/exclude_path.info` will be excluded for updating.
- `$ bash sync_accounts`
  - it will **interactively** ask you about `PW_SUFFIX` and `UPDATE_ALL_FILES`


## How to add users / groups
1. update proper .info files under sub-directory `infos` 
2. git commit & push

-------------------
## List of accounts
### Group
- 999: docker
- 30000: aisci

### Audio
range: 30001~30999
- 30001: jun3518
- 30002: kwkim
- 30003: hansw0326
- 30004: wbjung

### Vision
range: 31001~31999

### NLP
range: 32001~32999
- 32001: yjlee
- 32002: yjy2026
- 32003: jjl
- 32004: shwoo
- 32005: lukekim
- 32006: hylee

### Edge
range: 33001~33999

### Hall of the Expired
- ?????: swpark

---
### References
- https://stackoverflow.com/questions/1521462/looping-through-the-content-of-a-file-in-bash
- https://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash
- https://www.cyberciti.biz/faq/unix-howto-read-line-by-line-from-file/
- https://www.baeldung.com/linux/join-multiple-lines
- https://askubuntu.com/questions/29370/how-to-check-if-a-command-succeeded
